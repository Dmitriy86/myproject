let mainForm = document.getElementById('MyValidateForm');
    let submitBtn = document.getElementById('submit');
    let licenseAgreement = document.getElementById('agree');

    let checkIfEmpty = (elem) => {
      return elem.length === 0;
    }

    let checkSamePassword = (password, Pass) => {
      return password === Pass;
    }      

    licenseAgreement.addEventListener('click', function(e) {
      if (e.target.checked) {
        submitBtn.removeAttribute('disabled');
      } else {
        submitBtn.setAttribute('disabled', '');
      }
    })

    mainForm.addEventListener('submit', function(e) {
      e.preventDefault();

      let valid = true;

      if (checkIfEmpty(mainForm.login.value)) {
        valid = false;
        mainForm.login.className = 'error';
      } else {
        mainForm.login.className = 'OK';
      }

      if (checkIfEmpty(mainForm.pas1.value)) {
        valid = false;
        mainForm.pas1.className = 'error';
      } else {
        mainForm.pas1.className = 'OK';
      }

      if (!checkSamePassword(mainForm.pas1.value, mainForm.pas2.value)){
        valid = false;
        mainForm.pas2.className = 'error';
      } else {
        mainForm.pas2.className = 'OK';
      }     

      if (valid) {
        alert('form submit');
      }

    });
