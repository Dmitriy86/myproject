function Dog(name, poroda){
  this.name = name;
  this.poroda = poroda;
  this.status = 'idle';
}
let dogi = new Dog('Serg', 'American Dog');
console.log(dogi);
let dog = {
  name: 'Dingo',
  poroda: 'retriver',
  status: 'idle',
  changeStatus: function(){
      console.log('Собака ' + this.name + ' бежит. Собака ' + this.name + ' есть!');
  },
  showProps: function(){
      for(let key in dog){
          console.log(key, dog[key]);
      }
  },    
}
dog.changeStatus();
dog.showProps();
/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    Dog {
      name: '',
      name: '',
      status: 'idle',

      changeStatus: function(){...},
      showProps: function(){...}
    }

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/
