function chifr(string, step){
    let out = '';
    for(let i = 0; i < string.length; i ++){
        let code = string.charCodeAt(i);
        code = code + step;
        out = out + String.fromCharCode(code);
    }
    console.log('Шифр:', out);
    return out;
}
function deChifr(string, step){
    let out = '';
    for(let i = 0; i < string.length; i ++){
        let code = string.charCodeAt(i);
        code = code - step;
        out = out + String.fromCharCode(code);
    }
    console.log('Дешифр:',out);
}
let chifrKarring = chifr.bind(null,'Шифр Цезаря', 4);
let strResult1 = chifrKarring();
let deChifrKarring = deChifr.bind(null, strResult1, 4);
    deChifrKarring(); 
/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    const isMobile = document.innerWidth < 768;

    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/
