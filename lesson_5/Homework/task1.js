

function Commit(name, text, avatarUrl){
    this.name = name;
    this.text = text;
    this.avatarUrl = avatarUrl;
    this.likes = 0;
}
let CustomImg = {
    avatarUrl : '../../lesson_3/homework/images/cat1.jpg',
    countsLike : function(){
    this.likes ++;
    console.log(this.likes);
    },
}

let block = document.getElementById('CommentsFeed');
let commit1 = new Commit ('name_1', 'Commit1', CustomImg.avatarUrl);
let commit2 = new Commit ('name_2', 'Commit2', '../../lesson_3/homework/images/cat2.jpg');
let commit3 = new Commit ('name_3', 'Commit3', '../../lesson_3/homework/images/cat3.jpg');
let commit4 = new Commit ('name_4', 'Commit4', '../../lesson_3/homework/images/cat4.jpg');
let commits = [commit1, commit2, commit3, commit4]; 
function SecondConst(array){
    this.comments = array;
    this.comitsList = function(){
       this.comments.map(function(elem, item){
          Object.setPrototypeOf(elem, CustomImg);
          let nameCommit = document.createElement('h4');
          let imgCommit = document.createElement('img');
          let textCommit = document.createElement('p');
          let commitLikes = document.createElement('span');
              commitLikes.innerText = elem.likes;
              textCommit.innerText = elem.text;
              imgCommit.src = elem.avatarUrl;
              imgCommit.style.width = '100px';
              nameCommit.innerText = elem.name;
              block.appendChild(nameCommit);
              block.appendChild(imgCommit);
              block.appendChild(textCommit);
              block.appendChild(commitLikes);
              commitLikes.addEventListener('click', e => {
              elem.countsLike();
              commitLikes.innerText = elem.likes;
          });
            });
    }
  this.comitsList();
}

let comitListToDoc = new SecondConst(commits);

/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/