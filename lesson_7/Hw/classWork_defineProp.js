

/*
  Задание:

  Написать класс SuperDude который как аргумент принимает два параметра:
    - Имя
    - Массив суперспособностей которые являются обьектом.

    Модель суперспособности:
      {
        // Имя способности
        name:'Invisibility',
        // Сообщение которое будет выведено когда способность была вызвана
        spell: function(){ return `${this.name} hide from you`}
      }

    В конструкторе, нужно:
    - сделать так, что бы имя нельзя было перезаписывать и присвоить ему то
      значение которое мы передали как аргумент.

    - перебрать массив способностей и на каждую из них создать метод для этого
      обьекта, используя поле name как название метода, а spell как то,
      что нужно вернуть в console.log при вызове этого метода.
    - все способности должны быть неизменяемые

    - бонус, создать конструктор суперспособностей -> new Spell( name, spellFunc );
*/

  class SuperDude  {
      constructor(name, superPowers){
          this.name = name;
          this.superPowers = superPowers;
          Object.defineProperty(this, 'name', {
          value : this.name,
          writable : false,
          })
          this.superPowers.forEach(item => {
            Object.defineProperty(this, item.name, {
              value : () => console.log(item.spell()),
              writable : false,
            })  
          })
          
      }
  }
  let superPowers = [
    { name:'Invisibility', spell: function(){ return `${this.name} hide from you`} },
    { name:'superSpeed', spell: function(){ return `${this.name} running from you`} },
    { name:'superSight', spell: function(){ return `${this.name} see you`} },
    { name:'superFroze', spell: function(){ return `${this.name} will froze you`} },
    { name:'superSkin',  spell: function(){ return `${this.name} skin is unbreakable`} },
  ];
  // let result = superPowers.map(( elem, item) => {
  //       console.log(elem.name);
  //     if(elem.name == 'Invisibility') {
  //     SuperDude.prototype.Invisibility = function() {console.log(`${this.name}: `,elem.spell())};
  //     }
  //      else if(elem.name == 'superSpeed') {
        
  //     SuperDude.prototype.superSpeed = function() {console.log(`${this.name}: `,elem.spell())};
  //     }
  //     else if(elem.name == 'superSight') {
  //     SuperDude.prototype.superSight = function() {console.log(`${this.name}: `,elem.spell())};
  //     }
  //     else if(elem.name == 'superFroze') {
  //     SuperDude.prototype.superFroze = function() {console.log(`${this.name}: `,elem.spell())};
  //     }
  //     else if(elem.name == 'superSkin') {
  //     SuperDude.prototype.superSkin = function() {console.log(`${this.name}: `,elem.spell())};
  //     }

  // }) 

  let Luther = new SuperDude('Luther', superPowers);
  console.log(Luther);
  Luther.Invisibility();
  Luther.superSpeed();
  Luther.superSight();
  Luther.superFroze();
  Luther.superSkin();
  // Luther.Invisibility.apply(SuperDude);  
  
  // Тестирование: Методы должны работать и выводить сообщение.

      
      

