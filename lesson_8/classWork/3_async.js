 window.addEventListener('load',e => {
    async function getCompanies(){
    const getCompanyResponse = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
    const Companies = await getCompanyResponse.json();
    console.log(Companies);

    let tab = `
                <table>
                    <caption>Table Companies<caption>
                    <thead>
                        <tr>
                            <td>Company</td>
                            <td>Balance</td>
                            <td>Показать дату регистрации</td>
                            <td>Показать адресс</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                `
 document.body.innerHTML = tab;
 let tableFrom = document.querySelector('table tbody');
    tableFrom.style.textAlign = 'center';
 console.log(tableFrom);
 let find = Companies.filter((elem, item) => {
    for(let key in elem){
        return elem;
    }
 })

 console.log(find)
 let blockId = 0
 let sortTable = Companies.map( (elem, item) => {
    
    let strTable = document.createElement('tr');
        tableFrom.appendChild(strTable);
        let companyname = document.createElement('td');
            companyname.innerText = elem.company;
            strTable.appendChild(companyname)
        let balance = document.createElement('td');
            balance.innerText = elem.balance;
            strTable.appendChild(balance);
        let companyDate = document.createElement('td');
        let Text = document.createElement('div')
            Text.innerText = elem.registered;
            Text.setAttribute('style', 'display: none;')
            Text.setAttribute('id', blockId )
            Text.setAttribute('data-attr', 0 )
        let buttonReg = document.createElement('button');
            buttonReg.setAttribute('id', blockId )
            blockId ++;
            buttonReg.innerText = 'Показать дату регистрации';
            companyDate.appendChild(Text);
            companyDate.appendChild(buttonReg);
            strTable.appendChild(companyDate);
        let companyAddress = document.createElement('td');
        let TextAddr = document.createElement('div')
            TextAddr.setAttribute('style', 'display: none')
            TextAddr.setAttribute('id', blockId )
            TextAddr.setAttribute('data-attr', 0 )
        let buttonAddr = document.createElement('button');
            buttonAddr.setAttribute('id', blockId );
            blockId ++;
            buttonAddr.innerText = 'Показать адрес';
            companyAddress.style.width = '120px';
            console.log(elem.address);
            for(let key in elem.address){
               TextAddr.innerText += `${key}: ${elem.address[key]}, \n`; 
            }
            
            companyAddress.appendChild(TextAddr);
            companyAddress.appendChild(buttonAddr);
            strTable.appendChild(companyAddress);
                    
 })
 
const show = (e) => {
    let button = e.target;
     console.log(button.id);
    let block = document.getElementById(button.id);
    if(block.dataset.attr == 0){
        block.setAttribute('style', 'display: block')
        block.dataset.attr = 1;
        button.innreText = 'Скрыть дату регистрации!';
    }
    else if(block.dataset.attr == 1){
        block.dataset.attr = 0
        block.setAttribute('style', 'display: none')
        button.innreText = 'Показать дату регистрации!';
    }
} 
 let buttons = document.querySelectorAll('table td button')
 // console.log(buttons)
buttons.forEach( (elem, item) => {
    elem.addEventListener('click', show)
})

 }
 getCompanies() ; 

 })
 
 /*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
