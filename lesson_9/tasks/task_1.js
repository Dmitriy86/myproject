document.addEventListener('DOMContentLoaded', () => {
	let borderColor = document.querySelector('#borderColor');
      console.log(borderColor);
      let getColor = localStorage.getItem('color');
      document.body.style.background = getColor;
    const randomColor = () => {
      min = Math.ceil(1);
      max = Math.floor(255);
      let r = Math.floor(Math.random() * (max - min + 1)) + min;
      let g = Math.floor(Math.random() * (max - min + 1)) + min;
      let b = Math.floor(Math.random() * (max - min + 1)) + min;
      let color = '#' + r.toString(16) + '' + g.toString(16) + '' + b.toString(16) + '';
      if(color.length == 6) color = color + 1;
      localStorage.setItem('color',color);
      let getColor = localStorage.getItem('color');
      console.log(getColor);
      document.body.style.background = color;
	}
    borderColor.addEventListener( 'click', randomColor )
});
/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/
