let postForm = document.getElementById('postForm');
let postContainer = document.getElementById('postContainer');
let loadPostsBtn = postForm.querySelector('._loadPostsBtn');
let posts = [];

class Posts{
    constructor(_id, isActive, title, about, likes, created_at){
        this._id = _id ? _id : new Date();
        this.isActive = isActive;
        this.title = title;
        this.about = about;
        this.likes = likes ? likes : 0;
        this.created_at = created_at ? created_at : new Date();

        // this.render = this.render.bind(this);
        // this.addLike = this.addLike.bind(this);
    }
    render(){
        let block = document.createElement('div');
        block.dataset.id = this._id;
        block.classList.add('post');

        if (this.isActive) {
            block.classList.add('active');
        }

        var strContext = `            
                <i>Date: ${this.created_at}</i>
                <h3>Title: ${this.title}</h3>
                <p>${this.about}</p>
                <span class="like" >
                    <img src="like.png" alt="like" data-id="${this._id}">
                </span>                
                <i>likes: <span class="count" data-id="${this._id}">${this.likes}</span></i>            
            `;
        block.innerHTML = strContext;
        postContainer.prepend(block);
        // let likeBtn = block.querySelector('.like');
        // likeBtn.addEventListener('click', this.addLike);


    }
    addLike(el){
        console.log(posts)
        this.likes++;
        const like = document.querySelector(`[data-id="${el}"] .count`);
        console.log(like);
        const filtEl = posts.filter( (elem, index) => {
            if(like.dataset.id == elem._id){
                return elem;
            }
        })
        console.log(filtEl)

        like.innerHTML = filtEl[0].likes;
        // posts.forEach( elem => {
        //      if (elem._id === this._id) {
        //          elem.likes = this.likes;
        //      }
        //  }); 
        localStorage.setItem('posts', JSON.stringify(posts));
    
    }
}

async function fetchPost() {
    let respone = await fetch('http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2');
    let json = await respone.json();

    let currentPosts = GetSavedPosts();

    if (currentPosts !== null) {
        let uniquePosts = json.filter(function(obj) {
            return !currentPosts.some(function(obj2) {
                return obj._id == obj2._id;
            });
        });
    
        uniquePosts.forEach(post => {
            var post = new Posts(post.isActive, post.title, post.about, 0, post._id, post.created_at);   
            posts.push(post); 
        });
    } else {
        json.forEach(post => {
            var post = new Posts(post.isActive, post.title, post.about, 0, post._id, post.created_at);   
            posts.push(post);
        })
    }

    localStorage.setItem('posts', JSON.stringify(posts));
}

const GetSavedPosts = () => {
    let data = localStorage.getItem('posts');

    if (data !== null) {
        posts = JSON.parse(data);
        return posts;
    }
    return null;
}

postForm.addEventListener('submit', e => {
    e.preventDefault();
    let post = new Posts(new Date(),true,postForm.title.value, postForm.about.value,);
    posts.push(post);
    localStorage.setItem('posts', JSON.stringify(posts));
    post.render();
    postForm.reset();
    const likeBtn = document.querySelector('.like');
            console.log(likeBtn);
            likeBtn.addEventListener('click', e => {
            let elemData = e.target.dataset.id;
            console.log(elemData)
            post.addLike(elemData);
        })
});

loadPostsBtn.addEventListener('click', fetchPost);

let postsLS = GetSavedPosts();

if (postsLS !== null) {
    postsLS.forEach(function (post) {
        new Posts(post.isActive, post.title, post.about, post.likes, post._id, post.created_at);            
    })   

}


/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было предзагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/